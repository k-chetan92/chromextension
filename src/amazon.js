import {constants} from './utils/constants';
import {utils} from './utils/utils';
import TimeMe from 'timeme.js';

const url = new URL(window.location.href);

const getAsin = function(){
    const asin =  (document.getElementById("ASIN") && document.getElementById("ASIN").value) ||
        (new URL(window.location)).pathname.split('/')[3];
    return asin;
};




/**
 * This function clicks on the "Save for Later" button one by one.
 * Once it is done, it informs the background.js
 * @constructor
 */
const AddToSaveLater = function(newItems, cb) {
    console.log('AddToSaveLater');
    const toBeAdded = newItems;

    const functionRemove = function(){
        let link = document.querySelectorAll("[data-asin='"+ toBeAdded.pop() +"'] input[name*='submit.save-for-later']");

        if(link.length > 0) {
            link[0].click();
        }

        if(toBeAdded.length > 0) {
            setTimeout(function(){
                console.log("removed");
                functionRemove();
            }, 1000);

        }   else {
            setTimeout(function(){
                console.log("remove done");
                cb();
            }, 1500)
        }
    };

    functionRemove();

};

/**
 * Might not be able to extend this to a lot of links, but works for the links in the saved for later section
 * @param querySelector
 * @param action
 * @param extraParams
 */
const reportLinks = function(querySelector, action, extraParams) {

    const links = document.querySelectorAll(querySelector);

    links.forEach(el => {
        const asin = el.closest('[data-asin]').getAttribute('data-asin');

        const payload = {
            message: "productOperation",
            asin: asin,
            action: action
        };

        extraParams && Object.assign(payload, extraParams);

        el.addEventListener('click', function(){
            console.log("link track", asin, action);
            chrome.runtime.sendMessage(payload);
        });

    })
};

const getProductsInCart = function() {
    const items = [];
    const itemsCart = document.querySelectorAll("#sc-active-cart [data-asin]");

    itemsCart.forEach(el => {
        items.push(el.getAttribute("data-asin"))
    });

    return items;
};

const getProductsInSavedForLater = function() {
    const items = [];
    const itemsCart = document.querySelectorAll("#sc-saved-cart [data-asin]");

    itemsCart.forEach(el => {
        items.push(el.getAttribute("data-asin"))
    });

    return items;
};

const waitTimers = (function(){

    const timers = {};

    function Timer(asin, moveToCart, controlCondition) {
        if(controlCondition) {
            return;
        }
        const parent = document.createElement("div");
        parent.style.display = "inline-block";
        let created = null; let patience = null;
        const that = this;

        const defaultMessage = document.createElement("span");
        defaultMessage.append(document.createTextNode("Loading..."));
        defaultMessage.classList.add("loading");
        defaultMessage.style.display = "inline-block";
        defaultMessage.addEventListener("click", function(ev){
            ev.preventDefault();
            ev.stopPropagation();
        });
        parent.appendChild(defaultMessage);

        const timeRemaining = document.createElement("span");
        timeRemaining.classList.add("checkTime");
        timeRemaining.append(
            document.createTextNode("TIME REMAINING")
        );
        timeRemaining.classList.add("time");
        timeRemaining.style.display = "none";
        parent.appendChild(timeRemaining);

        parent.addEventListener("click", function(){

            chrome.runtime.sendMessage({
                message: "productOperation",
                asin: asin,
                action: "timeCheck"
            });

            if(Date.now() < (that.created + that.patience)) {
                //Show that there is pending time
                const timeRemaining_text = utils.timeToPhrase((that.created + that.patience) - Date.now());
                timeRemaining.textContent = (timeRemaining_text + " left.");
                timeRemaining.classList.remove("checkTime");
                setTimeout(function(){
                    timeRemaining.textContent = "TIME REMAINING";
                    timeRemaining.classList.add("checkTime");
                }, 5000)
            }   else {
                that.matured();
            }
        });

        moveToCart.style.display = "none";

        this.updateParameters = (c, p) => {
            that.created = c;
            that.patience = p;

            //Activate link automatically
            if(Date.now() < (that.created + that.patience)) {
                setTimeout(function(){
                    that.matured();
                }, (that.created + that.patience) - Date.now())
            }

        };

        this.loaded = () => {
            defaultMessage.style.display = "none";
            timeRemaining.style.display = "inline-block";
        };

        this.matured = () => {
            moveToCart.style.display = "inline-block";
            parent.style.display = "none";
        };

        this.element = parent;
    }

    const createTimer = function(asin, moveToCart, controlCondition){
        const timer = new Timer(asin, moveToCart, controlCondition);
        timers[asin] = timer;

        return timer;
    };

    return {
        timers,
        createTimer
    }

})();

const hidePremature = function(status) {
    console.log('hidePremature');
    const asins = Object.keys(status);
    let showAlert = false;

    asins.forEach(k => {9
        if(waitTimers.timers[k]) {
            if(status[k]) {
                const {created, patience} = status[k];
                waitTimers.timers[k].updateParameters(created, patience);
                waitTimers.timers[k].loaded();
                showAlert = true;
            }    else {
                waitTimers.timers[k].matured();
            }
        }
    });

    if(showAlert) {
        setTimeout(function(){
            window.alert("Items in your shopping cart have been moved to a Saved For Later list (shown below). Below each product there is a \"TIME REMAINING\" link. You can click this link to see when you can move products back to your shopping cart and, if you want to, checkout.")
        }, 0)
    }
};

function main(controlCondition) {

//Runs every time a page is loaded
(function(){


    if(!controlCondition) {
        document.getElementsByTagName('body')[0].classList.add("purchasepause")
    }

    //Logged into Prime? Checks the amazon logo on the nav bar.
    const tryPrime = !!document.getElementById("nav-logo") ? document.getElementById("nav-logo").querySelector('.nav-prime-try') : false;

    //Log the page and count time.
    chrome.runtime.sendMessage({url: window.location.href, prime: !tryPrime}, function(response) {
        const pageId = response;

        console.log("time start, ", pageId);

        //Idle time is set to 15 seconds, explanation: https://github.com/jasonzissman/TimeMe.js/
        //This library takes care of browser minimize and tav switches. Awesome.
        //The data from this might be a few seconds less owing to the load time.
        TimeMe.initialize({
            currentPageName: pageId,
            idleTimeoutInSeconds: 15
        });

        window.onbeforeunload = event => {
            const timeSpent = TimeMe.getTimeOnCurrentPageInSeconds();
            chrome.runtime.sendMessage({
                message: "pageLeave",
                pageId: pageId,
                timeSpent: timeSpent
            });
        }
    });

    const saveProductInfo = function(){
        //is this a project page? Check if some elements are there
        if(!!document.getElementById("productTitle") ||
            !!document.getElementById("dp-container") ||
            !!document.getElementById("ppd")) {
            const asin = getAsin();
            let title = document.getElementById("productTitle") ||
                document.getElementById("title_feature_div");

            if(!title) {
                title = document.querySelector("meta[name='title']").content
                title = title.replace("Amazon.com: ","");
            }   else    {
                title = title.innerText.trim();
            }


            let price = document.getElementById("priceblock_ourprice") ||
                document.getElementById("priceblock_dealprice") ||
                document.getElementById("guild_priceblock_ourprice") ||
                document.getElementById("offer-price a-text-normal") ||
                document.querySelector("[id^='dmusic_digital_buy_button_buyBox']") ||
                document.querySelector("[name='displayedPrice']") ||
                document.querySelector(".guild_priceblock_value") ||
                document.querySelector(".offer-price");

            if(price) {
                price = price.innerText || price.value;
                price = price.match(/([\d.])/g).join('');
                price = parseFloat(price);
            }   else {
                price = "not found";
            }


            const category_container = document.getElementById("wayfinding-breadcrumbs_container");
            const categories = [];
            const isPrime = !!(
                document.getElementById("prime_outer_div") ||
                document.getElementById("PrimeStripeContent") ||
                document.getElementsByClassName("primeUpsellIcon"));

            if(category_container) {
                const li = category_container.querySelectorAll("li");
                li.forEach(cat => {
                    if(!cat.classList.contains("a-breadcrumb-divider") && cat.id.indexOf("breadcrumb-back-link") === -1 && !cat.querySelector("#breadcrumb-back-link")) {
                        categories.push(cat.innerText.trim());
                    }
                })
            }

            const url = window.location.href;

            const info = {
                asin,
                title,
                price,
                isPrime,
                url,
                categories
            };

            console.log(info);

            chrome.runtime.sendMessage({message: "productFound", info: info});
        }
    };

    let currentAsin = null;

    setInterval(function () {
        const newAsin = getAsin();
        if(currentAsin !== newAsin) {
            currentAsin = newAsin;
            console.log("asin changed", newAsin);
            saveProductInfo();
        }
    }, 2000)



})();

/**
 * When in cart, hijack the delete, move to cart and wishlist links
 * Wishlist has link in a popup
 * Also call the DB to check when the product was added abd edit move to cart accordingly
 * Hide move to cart by default, as db call may take time.
 */
if(utils.isCart(url.href)) {

    chrome.runtime.sendMessage({message: 'productsMovedToCart'}, function(response){
        if(response) {
            const productsInCart = getProductsInCart();
            const productsSFL = getProductsInSavedForLater();
            const newItems = [];

            productsInCart.forEach(el => {
                //Also takes care of products which are added again.
                if(response.identified.indexOf(el) === -1 || productsSFL.indexOf(el) > -1) {
                    newItems.push(el);
                }
            });

            chrome.runtime.sendMessage({message: 'newItemsInCart', items: newItems});

            const cartStuff = function(){
                console.log('AddToSaveLater callback');
                //capture and report delete links
                reportLinks(constants.SFL_DELETE_SELECTOR, "deleted");

                //capture move to wishList
                reportLinks(constants.SFL_WISHLISTED_SELECTOR, "wishListed");

                //capture move to cart
                reportLinks(constants.SFL_CART_SELECTOR, "movedToCart");

                //capture move to cart
                reportLinks(constants.SFL_SAVE_LATER_SELECTOR, "savedForLater");

                console.log(document.querySelectorAll(constants.SFL_DELETE_SELECTOR));

                document.querySelectorAll(constants.SFL_DELETE_SELECTOR).forEach(el => {
                    el.addEventListener("click", event => {
                        const asin = event.target.closest("[data-asin]").getAttribute("data-asin");
                        console.log(asin, "deleted in local");
                        chrome.runtime.sendMessage({message: 'productDeleted', asin: asin});
                    })
                });

                let wishlistAsin = null;
                document.querySelectorAll(constants.SFL_WISHLISTED_SELECTOR).forEach(el => {
                    el.addEventListener("click", event => {
                        const asin = event.target.closest("[data-asin]").getAttribute("data-asin");
                        wishlistAsin = asin;
                    })
                });

                document.querySelectorAll("[data-registry-type]").forEach(el => {
                    el.addEventListener("click", event => {
                        if(wishlistAsin) {
                            console.log("added in wishlist");
                            chrome.runtime.sendMessage({message: 'productWishlist', asin: wishlistAsin});
                        }
                    })
                })

                //after adding, hide the move to cart

                if(!controlCondition) {
                    document.querySelectorAll("#sc-saved-cart .sc-list-body [data-asin]").forEach(el => {

                        if(!!el.querySelector(".sc-action-move-to-cart")) {
                            el.querySelector(".sc-action-move-to-cart").addEventListener('click', function (e) {
                                e.stopPropagation();
                            });
                            const moveToCart = el.querySelector("[value='Move to Cart']");
                            const elParent = el.querySelector(".sc-action-move-to-cart .a-declarative");
                            const asin = el.getAttribute("data-asin");
    
                            const timer = waitTimers.createTimer(asin, moveToCart, controlCondition).element;
                            elParent.insertBefore(timer, moveToCart);
                        }   else    {
                            console.log(el, " doesn't have a move to cart button.");
                        }
                    });
                }

                //control
                //Check time with DB
                chrome.runtime.sendMessage({message: 'getAddedProducts'}, function(response){
                    const asins = Object.keys(response);
                    const status = {};

                    //Decides if the object should be allowed to be purchased or not.
                    asins.forEach(el => {
                        const entries = Object.keys(response[el]);
                        let latestAdd = null;
                        entries.forEach(em => {
                            const dis = response[el][em];
                            if(dis["operation"] === "added") {

                                if(latestAdd === null || dis.timeStamp > latestAdd.timestamp) {
                                    latestAdd = dis;
                                }
                            }
                        });

                        if(!!latestAdd) {
                            if(Date.now() - latestAdd.timestamp > latestAdd.patience) {
                                console.log("product matured");
                                status[el] = false;
                            }    else {
                                console.log("product hidden");
                                status[el] = {
                                    created: latestAdd.timestamp,
                                    patience: latestAdd.patience
                                }
                            }
                        }

                    });

                    if(!controlCondition) {
                        hidePremature(status);
                    }
                });

            }

            if(!controlCondition) {
                AddToSaveLater(newItems, cartStuff);
            }   else {
                cartStuff();
            }

        }
    });

    //Add events to cart

    const checkoutButtons = document.querySelectorAll(".sc-proceed-to-checkout, .sc-1click-button");

    checkoutButtons.forEach(el => {
        el.addEventListener("click", event => {

            chrome.runtime.sendMessage({
                message: "productOperation",
                asin: getProductsInCart(),
                action: "checkout"
            });
        })
    });

    //Update details in cart

    document.querySelectorAll("#sc-active-cart [data-asin]").forEach(el => {
        const asin = el.getAttribute("data-asin");
        const quantity = el.getAttribute("data-quantity");

        chrome.runtime.sendMessage({message: 'updateQuantity',
        info: {
                asin,
                quantity
            }
        })
    });

    // Gather ASIN at checkout

    function attachCheckoutEvent() {
        if(document.getElementById("sc-buy-box-ptc-button")) {
            document.getElementById("sc-buy-box-ptc-button").addEventListener("click", ev => {
                debugger;
                const inCartProducts = [];

                document.querySelectorAll("#sc-active-cart [data-asin]").forEach(el => {
                    const asin = el.getAttribute("data-asin");
                    const title = el.querySelector(".sc-product-link span").innerText.trim();
                    inCartProducts.push({
                        asin,
                        title
                    });
                });

                console.log(inCartProducts);

                chrome.runtime.sendMessage({message: 'cartBeforeCheckout', products: inCartProducts});

            });
        }   else    {
            setTimeout(attachCheckoutEvent, 1000);
        }
    }

    attachCheckoutEvent();

}

if(utils.isCheckout(url.href)) {

    let asinList = [];
    chrome.runtime.sendMessage({message: 'getCartBeforeCheckout'}, data => {
        data.forEach(el => {
            asinList.push(el["asin"]);
        })
    });

    let buyTrigger = true;
    let shippingMethod = null;

    function attachPlaceOrder() {
        const buyButton = document.querySelectorAll(".place-order-button, .place-order-button-link");

        if(buyButton.length === 0) {
            setTimeout(attachPlaceOrder, 1000)
        }   else {
            buyButton.forEach(el => {
                el.addEventListener("click", ev => {
                    if(buyTrigger) {
                        buyTrigger = false;
                        debugger;
                        const payload = {
                            message: "productOperation",
                            asin: asinList,
                            action: "purchase"
                        };
                        chrome.runtime.sendMessage(payload)

                        setTimeout(() => {
                            buyTrigger = true;
                        }, 2000)
                    }
                });
            })
        }
    }

    attachPlaceOrder();

    setInterval(function(){
        const inputs = document.querySelectorAll('.shipping-speeds input[type="radio"]');
        inputs.forEach(el => {
            if(el.checked) {
                console.log(el.title);
                if(shippingMethod === null) {
                    shippingMethod = el.title;
                }   else if(shippingMethod !== el.title) {
                    shippingMethod = el.title;
                    setTimeout(attachPlaceOrder, 1500)
                }
            }
        })
    }, 1000)
}

if(utils.isWishlist(url.href)) {
    //Add listeners for delete and add to cart

    //Delete here refers to delete from wishlist, hence removing the event listeners
/*    const deleteButtons = document.querySelectorAll("[data-reg-item-delete]");
    if(deleteButtons) {
        deleteButtons.forEach(el => {
            el.addEventListener("click", function (ev) {
                try{
                    const asin = JSON.parse(
                        el.getAttribute("data-reg-item-delete")
                    )["asin"];

                    const payload = {
                        message: "productOperation",
                        asin: asin,
                        action: "deleted",
                        timestamp: Date.now(),
                    };


                    el.addEventListener('click', function(){
                        console.log("link track", asin, "deleted");
                        chrome.runtime.sendMessage(payload);
                    });

                }   catch(err) {
                    console.log(err);
                }
            })
        })
    }*/

    const addedButtons = document.querySelectorAll("[data-add-to-cart]");
    if(addedButtons) {
        addedButtons.forEach(el => {
            el.addEventListener("click", function (ev) {
                try{
                    const asin = JSON.parse(
                        el.getAttribute("data-add-to-cart")
                    )["asin"];

                    chrome.runtime.sendMessage({message: 'productAdded', "asin": asin}, function(){});
                    console.log("asin, ", asin);

                }   catch(err) {
                    console.log(err);
                }
            })
        })
    }
}

/**
 * Text replace or hide on the Added to Cart page
 */
if(utils.isATCpage(url.href)) {
    //Check the text elements on page and change it

    const replacementPairs = [
        ["#huc-v2-order-row-confirm-text h1", "Saved for later"],
        ["#hl-confirm-text #confirm-text", "Saved for later"]
    ];

    const hideElements = [
        "#hlb-ptc-btn",
        "#hlb-cs-btn",
        "#hlb-next-steps .hlb-checkout-button"
    ];

    if(!controlCondition) {
        replacementPairs.forEach(function(el){
            let a = document.querySelectorAll(el[0]);
    
            if(a[0]) {
                a[0].innerText = el[1];
            }
        });
    
        hideElements.forEach(function(el) {
            let a = document.querySelectorAll(el);
            if(a[0]) {
                a[0].style.display = "none";
            }
        });
    }

    //Add a product in DB

    let asinContainer = document.querySelectorAll("[data-a-popover]");
    let asins = [];

    asinContainer.forEach(el => {
        if(el && typeof el.getAttribute('data-a-popover') == "string") {
            let asin = JSON.parse(el.getAttribute('data-a-popover')).name.split('-')[2];
            asins.push(asin);
            console.log("asin", asin);
        }
    });

    if(asins.length) {
        chrome.runtime.sendMessage({message: 'productAdded', "asin": asins}, function(){});
    }

    if(!asinContainer.length) {
        const asinContainer = document.getElementsByClassName("hlb-item-link");
        const asins = [];
        if(asinContainer) {

            for(let el = 0, j = asinContainer.length; el<j; el++) {
                if(asinContainer[el]) {
                    const asin = asinContainer[el].getAttribute("class").split("hlb-asin-")[1];
                    asins.push(asin);

                    console.log("asin", asin)
                }
            }
        }

        chrome.runtime.sendMessage({message: 'productAdded', "asin": asins}, function(){});
    }
}

/**
 * Some pages have "Added to cart page" as a panel. This takes care of that.
 * Have to poll for changes
 */
if(document.getElementById("ccxsmartshelf_secondaryPanel")) {
    let checkAdded = function() {
        let panelClass = document.getElementById("ccxsmartshelf_secondaryPanel").className;

        if(panelClass.indexOf("aok-hidden") == -1) {
            let asin = getAsin();
            console.log("asin", asin);

            chrome.runtime.sendMessage({message: 'productAdded', "asin": asin}, function(){});

        }   else {
            console.log("panel page; not added yet");
            setTimeout(checkAdded, 500);
        }

    };

    checkAdded();
}


/**
 * Another variant of the panel. :(
 */

if(document.getElementById("attach-dss-placeholder")) {

    let checkAdded = function() {

        const added_cart = document.querySelectorAll("#attach-accessory-pane, #attach-warranty-pane");
        const panel_open = (added_cart[0] && added_cart[0].getBoundingClientRect().height > 0) || (added_cart[1] && added_cart[1].getBoundingClientRect().height > 0)

        if(panel_open) {
            let asin = getAsin();
            console.log("asin", asin);
            chrome.runtime.sendMessage({message: 'productAdded', "asin": [asin]}, function(){});

            const coverage = document.getElementById("attachSiAddCoverage-announce");

            coverage ? coverage.addEventListener("click", el => {
                let asin = el.target.getAttribute('data-asin');
                if(asin) {
                    console.log("asin", asin);
                    chrome.runtime.sendMessage({message: 'productAdded', "asin": [asin]}, function(){});
                }
            }) : null;

        }   else {
            console.log("large panel page; not added yet; polling");
            setTimeout(checkAdded, 500);
        }

    };

    checkAdded();
}

/**
 * Lightning Deal's page
 */



/**
 * Today's deals page
 */


if(document.querySelectorAll("[id*=dealView]")) {

        function hookClickListeners() {
            document.querySelectorAll("[id*=dealView]").forEach(el => {

                if(!el.getAttribute("hooked")) {

                    el.setAttribute("hooked","yes");
                    el.addEventListener("click", ev => {

                        try{
                            const asin = JSON.parse(el.querySelector("[data-gbdeal-addtocart]").getAttribute("data-gbdeal-addtocart"))["asin"];

                            if((ev.target.innerText === "Add to Cart") && (ev.target.classList.contains("a-button-text") || ev.target.querySelector(".a-button-text"))) {
                                chrome.runtime.sendMessage({message: 'productAdded', "asin": asin}, function(){});
                                console.log("asin, ", asin);
                            }
                        } catch (err) {
                            console.log("asin not found");
                        }

                    });
                }

            });

        }

        function hookPopover() {
            const popovers = document.getElementsByClassName("a-popover-inner")

            if(popovers.length > 0) {
                for(let i=0, j = popovers.length; i<j; i++) {
                    const el = popovers[i];

                    if(!el.getAttribute("hooked")) {
                        el.setAttribute("hooked","yes");

                        el.addEventListener("click", ev => {
                            try {
                                if((ev.target.innerText === "Add to Cart") &&
                                    ((ev.target.classList.contains("a-button-text")) ||
                                        ev.target.querySelector(".a-button-text")) &&
                                    !((ev.target.classList.contains("a-button-disabled")) ||
                                        ev.target.querySelector(".a-button-disabled"))
                                ){
                                    const asin = JSON.parse(el.querySelector("[data-gbdeal-addtocart]").getAttribute("data-gbdeal-addtocart"))["asin"];
                                    chrome.runtime.sendMessage({message: 'productAdded', "asin": asin}, function(){});
                                    console.log("asin, ", asin);
                                }
                                else {

                                }
                            } catch(err) {

                            }
                        })
                    }
                }
            }
        }

        //Today's deal is a fancy single page application
        //Need to poll for new products which appear via pagination.
        setInterval(function(){
            hookClickListeners();
            hookPopover();
        }, 1000);

}

// Amazon Pantry promotion page

if(document.querySelector("[data-p-prod-tile-atc]")) {
    
    function hookClickListeners() {
        
        const cartCTA = document.querySelectorAll("[data-p-prod-tile-atc]");
        cartCTA.forEach(el => {
            if(!el.getAttribute("hooked")) {
                el.setAttribute("hooked", "yes");

                el.addEventListener("click", ev => {
                    try {
                        const asin = JSON.parse(el.getAttribute("data-p-prod-tile-atc"))["asin"];
                        if(ev.target.innerText === "Add to Cart"  || 
                            ev.target.classList.contains("a-button-text") || 
                            ev.target.classList.contains("a-button-inner") || 
                            ev.target.classList.contains("a-button-input")) {
                            chrome.runtime.sendMessage({message: 'productAdded', "asin": asin}, function(){});
                            console.log("asin, ", asin);
                        }
                    } catch (err) {
                        console.log("Asin not found.");
                    }
                })
            }
        })
    }

    setInterval(function(){
        hookClickListeners();
    }, 1000);

}

// Amazon Pantry promotion page

if(document.querySelector("[data-pantry-add-to-cart]")) {
    
    function hookClickListeners() {
        
        const cartCTA = document.querySelectorAll("[data-pantry-add-to-cart]");
        cartCTA.forEach(el => {
            if(!el.getAttribute("hooked")) {
                el.setAttribute("hooked", "yes");

                el.addEventListener("click", ev => {
                    try {
                        const parent = ev.target.closest(".pantryItemContents");
                        const asin = parent.getAttribute("id").replace("pantryItemContents_", "")
                        if(ev.target.innerText === "Add to Cart"  || 
                            ev.target.classList.contains("a-button-text") || 
                            ev.target.classList.contains("a-button-inner") || 
                            ev.target.classList.contains("a-button-input")) {
                            chrome.runtime.sendMessage({message: 'productAdded', "asin": asin}, function(){ console.log("asin, ", asin) });
                        }
                    } catch (err) {
                        console.log("Asin not found.");
                    }
                })
            }
        })
    }

    setInterval(function(){
        hookClickListeners();
    }, 1000);

}

//added event for buy with one click, works for those in control condition
let buyNowAllowed = true;
if(document.querySelector(".a-button-oneclick, #buy-now-button, #submit.buy-now, #oneClickBuyButton")) {
    const buynow = document.querySelectorAll(".a-button-oneclick, #buy-now-button, #submit.buy-now, #oneClickBuyButton")
        buynow.forEach(el => {
            el.addEventListener("click", ev => {
                if(buyNowAllowed) {
                    const asin = getAsin();
                    const payload = {
                        message: "productOperation",
                        asin: [asin],
                        action: "purchase"
                    };
                    chrome.runtime.sendMessage(payload);
                    buyNowAllowed = false;
                    setTimeout(function(){
                        buyNowAllowed = true;
                    }, 3000)
                }   else {
                    console.log("buy now double click was observed");
                }
            })
        })
    }
}

chrome.runtime.sendMessage({message: "getControlCondition"}, function(response) {
    console.log("ControlCondition, ", response)
    main(response);
})
