import {constants} from './constants';
import * as firebase from 'firebase';

const config = {
    apiKey: '<apiKey>',
    databaseURL: '<databaseURL>',
    storageBucket: '<storageBucket>'
};

firebase.initializeApp(config);

module.exports.utils = (function (constants){

    const isPage = function(url, set, classSet) {
        if(!url) {
            return false;
        }

        let flag = false;

        set.forEach(function(el) {
            if(url.indexOf(el) > -1) {
                flag = true;
            }
        });

        if(classSet) {
            flag = flag || (document.querySelectorAll(classSet.join(", ")).length > 0);
        }

        return flag
    };

    //Currying?
    const isATCpage = function(url) {
        return isPage(url, constants.ADDED_TO_CART)
    };

    //in some occasions, add to cart URL persists for the cart page.
    const isCart = function(url) {
        return isPage(url, constants.CART_URL, constants.CART_CLASSES)
    };

    const isWishlist = function (url) {
        return isPage(url, constants.WISHLIST_URL, constants.WISHLIST_CLASSES)
    }

    const isCheckout = function(url) {
        return isPage(url, constants.CHECKOUT_URL, constants.CHECKOUT_CLASSES)
    };

    const timeToPhrase = function(timePending) {
        const time = new Date(timePending);
        let str = "";

        if(time.getUTCDate()-1 > 0) {
            str += (time.getUTCDate()-1) + " days, ";
        }

        if(time.getUTCHours() > 0) {
            str += (time.getUTCHours()) + " hours, ";
        }

        if(time.getUTCMinutes() > 0) {
            str += (time.getUTCMinutes()) + " minutes, ";
        }

        if(time.getUTCSeconds() > 0) {
            str += (time.getUTCSeconds()) + " seconds";
        }

        return str;
    };

    const db = (function(){
        const firebae = firebase.database();

        const push = function(path, data) {
            const ref = firebae.ref(path).push();
            ref.set(data);
            return ref.key;
        };

        const set = function(path, data) {
            const ref = firebae.ref(path);
            return ref.set(data);
        };

        const get = function(path, cb) {
            const ref = firebae.ref(path);
            ref.once('value').then(cb);
        };

        const update = function(path, data) {
            const ref = firebae.ref(path);
            ref.update(data);
        };

        const remove = function(path, cb) {
            const ref = firebae.ref(path);
            ref.remove().then(cb);
        };

        return {
            push,
            set,
            get,
            update,
            remove
        }
    })();

    return {
        isATCpage,
        isCart,
        isCheckout,
        isWishlist,
        timeToPhrase,
        db
    }
})(constants);

module.exports.firebase = firebase;