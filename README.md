# Purchase Pause

A chrome extension which prevents users from making impulse purchases. The extension prevents you from checking out immediately, giving you time to reflect on your spending behavior.

The extension is written in Javascript, and uses a serverless backend written in JS as well. We use firebase to save data in a NoSQL DB. A script is used to then assimilate the data and download as an excel file.

The project scrapes the markup for order status, product details etc. This most challenging part of the project. Accounting for variations in the possible markup is time consuming and error prone. Next time, I can consider using TypeScript for such projects.

### Installing

This project consists of two repositories.

https://github.com/kchetan92/pause-purchase-chrome
https://github.com/kchetan92/pause-purchase-cloud

-chrome is the chrome extensions and -cloud is the server code.

### Chrome extension

1. Install node and npm from https://nodejs.org/en/download/. There are plenty of resources online to help you in case you face any trouble installing.
2. Copy the repository using the following command in terminal:
```sh
git clone https://github.com/kchetan92/pause-purchase-chrome.git
```
3. Use npm install to download all the dependencies. This can take a while. Node has a brilliant package manager but it is infamous for having so many dependencies.
4. For the next step, we would need some keys. Headover to firebase.google.com, go to the console and make a new project. (If you already have the keys from the developer, use those instead, skip to step 6)
5. On the home screen of firebase, click on “Add Firebase to your web app”, you’ll see an object with some keys and URLs. You’ll need this for the next step.
6. Make a copy of keys-example.json and rename it to keys.json.
7. Copy the corresponding value from your firebase dashboard and replace the placeholder text with actual URL and keys.
8. In terminal at the root directory of the repository, run: gulp (If this doesn’t work install gulp globally: npm install --global gulp ; and try again)
This should generate a build folder.
9. Open chrome and go to chrome://extensions/ , switch on the developer mode on the top right.
10. Click on load unpacked and select the build folder. Tadaa! But wait we don’t have a backend to send the data to.

### Firebase Backend
1. Clone the repository in a new folder, using this command:
```sh
https://github.com/kchetan92/pause-purchase-cloud.git
```
2. Install the firebase tools
```
npm install -g firebase-tools
```
3. Login to your gmail account
```
firebase login
```
4. Navigate to repo you just cloned and initialize firebase using
```
firebase init
```
5. Choose the project you just created when asked.
6. When asked to choose the modules, select hosting, database and functions.
7. Do not override any of the existing files.
8. Choose default for others (indicated by capital letter in y/n).
9. Make changes and deploy to the server using
```
firebase deploy
```
