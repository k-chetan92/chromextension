var gulp = require("gulp");
var del = require("del");
var replace = require("gulp-batch-replace");
var browserify = require('browserify');
var babelify = require('babelify');
var keys = require("./keys.json");
var tap = require("gulp-tap");
var uglify = require("gulp-uglify");

var source = require("vinyl-source-stream");
var buffer = require("vinyl-buffer");

gulp.task("clean", function(){
	return del(["build"])
});

gulp.task("copy", function(){
	return gulp.src(['src/**/*.*', '!src/**/*.js'])
		.pipe(gulp.dest('build'))
});


gulp.task("copy-replace", function(){
    return gulp.src('src/*.js', {read: false})

        .pipe(tap(function(file) {
            file.contents = browserify(file.path, {
                debug: true,
                transform: [babelify.configure({
                    presets: ['es2015']
                })]
            }).bundle();
        }))
        .pipe(buffer())
        .pipe(replace(keys.keys))
        .pipe(gulp.dest('./build/'));
});

//repetition, fix it later.
gulp.task("copy-replace-pack", function(){
    return gulp.src('src/*.js', {read: false})

        .pipe(tap(function(file) {
            file.contents = browserify(file.path, {
                debug: true,
                transform: [babelify.configure({
                    presets: ['es2015']
                })]
            }).bundle();
        }))
        .pipe(buffer())
        .pipe(replace(keys.keys))
        .pipe(uglify())
        .pipe(gulp.dest('./build/'));
});

gulp.watch('src/**/*.*', ["copy", "copy-replace"]);

gulp.task('default', ["copy", "copy-replace"]);

gulp.task('build', ["copy", "copy-replace-pack"]);