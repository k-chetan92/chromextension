import {utils, firebase} from './utils/utils';

let user = null;
let controlCondition = false;

const connectToPage = (function(){

    //Add a listener of messages from the on page script

    chrome.runtime.onMessage.addListener( function(request, sender, sendResponse) {

            if(!user) {
                if(request && request.user) {
                    user = request.user;
                } else {
                    sendResponse(null);
                    return;
                }
            }

            console.log("req ", request);


            if (request.url) {
                sendResponse( utils.db.push('users/' + user.uid + '/pages', {
                    "url": request.url,
                    "time": Date.now(),
                    "prime": request.prime
                }));

                return true
            }

            if(request.message === "signuptime") {
                debugger;
                utils.db.set('signuptime/' + user.uid, {
                    time: Date.now(),
                    email: user.email
                })
            }

            if(request.message === "getControlCondition") {
                debugger;
                chrome.storage.local.get(['controlCondition'], result => {
                    if(Object.keys(result).length === 0 || result['controlCondition'] === "refresh") {
                        utils.db.get('users/' + user.uid + '/data/', function(data) {
                            let category = data.val() ? data.val()["patienceCategory"] : null;
                            if(typeof category !== 'undefined' && category !== null) {
                                utils.db.get('patience/' + category, function(data) {
                                    var control = data.val()["control"];
                                    chrome.storage.local.set({'controlCondition': control});
                                    sendResponse(control);
                                })
                            }
                        });
                    }   else {
                        sendResponse(result['controlCondition']);
                    }
                });

                return true;
            }

            if(request.message === "pageLeave") {
                utils.db.update('users/' + user.uid + '/pages/' + request.pageId, {

                    //Converting to ms for consistency
                    timeSpent: request.timeSpent*1000
                })
            }

            if(request.message === "productFound") {
                utils.db.update('users/' + user.uid + '/product_data/' + request.info.asin, request.info)
            }


            if(request.message === "productAdded") {

                //to accomodate for multiple operation.
                if(!Array.isArray(request.asin)) {
                    request.asin = [request.asin];
                }

                request.asin.forEach(num => {
                    utils.db.push('users/' + user.uid + '/product/' + num, {
                        "asin": num,
                        "timestamp": Date.now(),
                        "operation": "added"
                    });

                    setInterval(function(){}, 500);
                });

            }

            if(request.message === "productOperation") {

                //to accomodate for multiple operation.
                if(!Array.isArray(request.asin)) {
                    request.asin = [request.asin];
                }

                request.asin.forEach(num => {
                    utils.db.push('users/' + user.uid + '/product/' + num, {
                        "timestamp": request.timestamp || Date.now(),
                        "operation": request.action,
                        "asin": num
                    });

                    // Something goes wrong when entries are added after another.
                    setTimeout(function(){}, 500);
                });

            }

            if(request.message === "getAddedProducts") {
                utils.db.get('users/' + user.uid + '/product/', function(data) {
                    sendResponse(data.val());
                });

                //https://developer.chrome.com/apps/messaging
                return true;
            }

            if(request.message === "productsMovedToCart") {
                chrome.storage.local.get(['identified'], result => {
                    if(Object.keys(result).length === 0) {
                        chrome.storage.local.set({'identified':[]});
                        sendResponse({'identified':[]});
                    }   else {
                        sendResponse(result);
                    }
                });

                return true;
            }

            if(request.message === "newItemsInCart") {
                chrome.storage.local.get(['identified'], result => {
                    if(Object.keys(result).length === 0) {
                        chrome.storage.local.set({'identified':[]});
                        sendResponse([]);
                    }   else {
                        chrome.storage.local.set({
                            'identified': result.identified.concat(request.items)
                        });
                    }
                });

                return true;
            }

            if(request.message === "productDeleted" || request.message === "productWishlist") {
                chrome.storage.local.get(['identified'], result => {
                    //remove the recently deleted value
                    const newArray = [];
                    result.identified.forEach(el => {
                        if(el !== request.asin) {
                            newArray.push(el);
                        }
                    });

                    chrome.storage.local.set({
                        'identified': newArray
                    });
                })
            }

            if(request.message === "updateQuantity") {
                utils.db.update('users/' + user.uid + '/product_data/' + request.info.asin, {
                    quantity: request.info.quantity
                });
            }

            if(request.message === "cartBeforeCheckout") {
                chrome.storage.local.set({
                    'cartBeforeCheckout': request.products
                });
            }

            if(request.message === "getCartBeforeCheckout") {
                chrome.storage.local.get(["cartBeforeCheckout"],result => {
                    sendResponse(result["cartBeforeCheckout"]);
                });

                return true;
            }

            if(request.message === "getConsent") {

                utils.db.get('consent/' + user.uid , function(snapshot, context) {
                    if(snapshot.exists()) {
                        sendResponse(true);
                    }   else    {
                        sendResponse(false);
                    }
                });

                return true;
            }

            if(request.message === "emailVerified") {
                return user.emailVerified;
            }

            if(request.message === "submitConsent") {
                utils.db.set('consent/' + user.uid, {
                    consent: request.consent,
                    time: Date.now()
                })
                .then(function(){
                    sendResponse(true);
                })
                .catch(function(err){
                    console.log(err);
                    sendResponse(false);
                });

                return true
            }



            //This should be used only for development purpose
            if(request.message === "clear-history") {
                chrome.storage.local.set({'identified':[]});
                utils.db.remove('users/' + user.uid + '/', function() {
                    sendResponse(true);
                });

                return true;
            }


        });

    chrome.runtime.onInstalled.addListener(function(details){
        chrome.tabs.create({
            url: "http://amazon.com"
        })
        setTimeout(() => {
            window.alert("Thank you for downloading Purchase Pause. Click on the red Purchase Pause icon next to your search bar to create an account.")
        }, 2000)
    })
})()

function initApp() {    
  // Listen for auth state changes.
  firebase.auth().onIdTokenChanged(function(usr) {
    console.log('User state change detected from the Background script of the Chrome Extension:', usr);

    //connectToPage(user);

    user = usr;

  });
};

window.onload = function() {
  initApp();
};
