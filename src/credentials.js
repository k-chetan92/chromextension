// Initialize Firebase
var config = {
  apiKey: '<apiKey>',
  databaseURL: '<databaseURL>',
  storageBucket: '<storageBucket>'
};
firebase.initializeApp(config);
var database = firebase.database();

const PASSWORD = "pastWord@190";

function isEmail(email){
  return /^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*$/.test( email );  
}

function setState(classname) {
  const parent = document.getElementById("container");
  parent.setAttribute("class","");
  if(classname) {
    parent.classList.add(classname);
  }
}

function showError(error) {
  console.log(error);
  const errorMessage = document.getElementById("error-message");
  let message = "";
  if(typeof error == "string") {
    message = error
  } else{
    message = JSON.stringify(error);
  }
  errorMessage.innerText = message;
  setTimeout(function(){
    errorMessage.innerText = "";
  }, 5000);
}

function initApp() {

  const userName = document.getElementById("hello");
  // const askVerifyEmail = document.getElementById("verify-email");
  const consentForm = document.getElementById("consent-form");
  const googleSignIn = document.querySelector("#login-methods .google");
  const loading = document.getElementById("loading");
  const agreeButton = document.getElementById("agree-toc");
  const agreedMessage = document.getElementById("agreed-toc");
  const disagreeMessage = document.getElementById("disagree-toc");
  const signOut = document.getElementById("sign-out");

  const signUpOption = document.querySelector("#login-methods .signup");
  const signUpForm = document.querySelector("#sign-up-form form");
  const loginForm = document.querySelector("#login-form form");
  //const forgotPassword = document.querySelector("#forgot-password")
  const clearHistory = document.querySelector("#clear-history")

  const loginOpiton = document.querySelector("#login-methods .login");

  let newName = ""

  //event Listeners

  let setUser = null;

  signOut.addEventListener("click", function(){
    chrome.storage.local.set({'controlCondition': "refresh"});
    firebase.auth().signOut();
  });

  // askVerifyEmail.querySelector("a").addEventListener("click", function(){
  //   if(setUser) {
  //     setUser.sendEmailVerification().then(function() {
  //       showError("sent")
  //     }).catch(function(error) {
  //       showError(error);
  //     });
  //   }
  // });

  agreeButton.addEventListener("click", function(){
    loading.style.display = "block";
    chrome.runtime.sendMessage({message: "submitConsent", consent: true}, function (data) {
      loading.style.display = "none";
      if(data) {
        agreedMessage.style.display = "block";
        document.getElementById("toc-message").style.display = "none";
        agreeButton.style.display = "none";
        disagreeMessage.style.display = "none";
      }
    });
  });

  signUpForm.addEventListener("submit", function(e){
    e.preventDefault();
    loading.style.display = "block";
    // const name = signUpForm.querySelector("[name='first_name']").value;
     // + " " + signUpForm.querySelector("[name='last_name']").value;

    const email = signUpForm.querySelector("[name='email']").value;
    const verifyEmail = signUpForm.querySelector("[name='verify_email']").value;

    // if(!isEmail(email)) {
    //   loading.style.display = "none";
    //   showError(err.message || err || "something went wrong");
    //   return;
    // }

    if(email !== verifyEmail) {
      showError("Email address did not match");
      return;
    }

    firebase.auth().createUserWithEmailAndPassword(email, PASSWORD)
    .then(user => {
      // if(!user.emailVerified) {
      //   user.sendEmailVerification().then(function() {
      //     console.log("email sent");
      //     showError("Check your mail for verification email. Check SPAM folder as well.");
      //   }).catch(function(err) {
      //     showError(err);
      //     console.log(err);
      //   }).finally(function() {
      //     loading.style.display = "none";
      //   });
      // };
      //TODO: Send email confirmation

      loading.style.display = "none";

      // user.updateProfile({
      //   displayName: name
      // }).then(el => {
      //   console.log("done");
      // }).catch(err => {
      //   showError(err);
      // })

      chrome.runtime.sendMessage({message: "signuptime", user: user});

    })
    .catch(function(err) {
      showError(err.message || err || "something went wrong");
    });
  });

  loginForm.addEventListener("submit", function(e){
    e.preventDefault();
    loading.style.display = "block";
    const email = loginForm.querySelector("[name='email']").value;
    // const password = loginForm.querySelector("[name='password']").value;

    firebase.auth().signInWithEmailAndPassword(email, PASSWORD).then(() => {
      setState("loggedIn")
    }).catch(err => {
      showError(err.message || err || "something went wrong");
    }).finally(function() {
      loading.style.display = "none";
    })
  })

  // forgotPassword.addEventListener("click", function(e){
  //   var email = loginForm.querySelector("[name='email']").value;
  //   loading.style.display = "block";
    
  //   firebase.auth().sendPasswordResetEmail(email)
  //     .then(function(){
  //       showError("check your email");
  //     })
  //     .catch(function(err) {
  //       showError(err.message || err || "something went wrong");
  //     })
  //     .finally(function(){
  //       loading.style.display = "none";
  //     })

  // })

  loginOpiton.addEventListener("click", function() {
    setState("login");
    firebase.auth().signOut();
  })


  signUpOption.addEventListener("click", function(){
    setState("signUp");
    firebase.auth().signOut();
  })

  clearHistory.addEventListener("click", function(){
    chrome.runtime.sendMessage({message: "clear-history"})
  })

  // Listen for auth state changes.
  // [START authstatelistener]
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      loading.style.display = "none";
      setUser = user;
      setState("loggedIn");
      // User is signed in.

      // if(displayName.indexOf(" ") > -1) {
      //   //use the first name
      //   displayName = displayName.split(" ")[0]
      // }

      var email = user.email;
      var emailVerified = user.emailVerified;
      var photoURL = user.photoURL;
      var isAnonymous = user.isAnonymous;
      var uid = user.uid;
      var providerData = user.providerData;

      userName.innerText = "Logged in as " + (email || "");

      // if(!emailVerified) {
      //   askVerifyEmail.style.display = "block";
      // }

      // setTimeout(function(){

      //   chrome.runtime.sendMessage({message: "getConsent"}, function (data) {
      //     if(!data) {
      //       consentForm.style.display = "block";
      //     }
      //   });
      // }, 1500)

      // [END_EXCLUDE]
    } else {
      setState("NotLoggedIn");
      loading.style.display = "none";
      // Let's try to get a Google auth token programmatically.
      // [START_EXCLUDE]

      // googleSignIn.addEventListener("click", function(){
      //   loading.style.display = "block";
      //   startSignIn();
      // })

      // [END_EXCLUDE]
    }
    //document.getElementById('quickstart-button').disabled = false;
  });
  // [END authstatelistener]

  //document.getElementById('quickstart-button').addEventListener('click', startSignIn, false);

}

/**
 * Start the auth flow and authorizes to Firebase.
 * @param{boolean} interactive True if the OAuth flow should request with an interactive mode.
 */
function startAuth(interactive) {
  // Request an OAuth token from the Chrome Identity API.
  chrome.identity.getAuthToken({interactive: !!interactive}, function(token) {
    if (chrome.runtime.lastError && !interactive) {
      console.log('It was not possible to get a token programmatically.');
    } else if(chrome.runtime.lastError) {
      console.error(chrome.runtime.lastError);
    } else if (token) {
      // Authorize Firebase with the OAuth Access Token.
      var credential = firebase.auth.GoogleAuthProvider.credential(null, token);
      firebase.auth().signInWithCredential(credential).catch(function(error) {
        // The OAuth token might have been invalidated. Lets' remove it from cache.
        if (error.code === 'auth/invalid-credential') {
          chrome.identity.removeCachedAuthToken({token: token}, function() {
            startAuth(interactive);
          });
        }
      });

    } else {
      console.error('The OAuth Token was null');
    }
  });
}

/**
 * Starts the sign-in process.
 */
function startSignIn() {
  //document.getElementById('quickstart-button').disabled = true;
  if (firebase.auth().currentUser) {
    firebase.auth().signOut();
  } else {
    startAuth(true);
  }
}

window.onload = function() {
  initApp();
};
