module.exports.constants = {

    //Different URL patterns were found
    ADDED_TO_CART : [
        "/gp/item-dispatch",
        "/gp/huc",
        "/gp/product/handle-buy-box"
    ],

    //Redirect to this page once something has been added to the cart
    POST_ADD_URL: "https://www.amazon.com/gp/cart/view.html?todo=addSaveLater",

    CART_URL: ["/gp/cart/view.html"],

    CART_CLASSES: ["#sc-saved-cart", "#savedCartViewForm"],

    CHECKOUT_URL: ["gp/buy/spc"],

    WISHLIST_URL: ["gp/registry/wishlist"],

    WISHLIST_CLASSES: ["#my-lists-tab"],

    CHECKOUT_CLASSES: ["#checkoutDisplayPage"],

    SFL_DELETE_SELECTOR: ".sc-action-delete [value='Delete']",

    SFL_CART_SELECTOR: ".sc-action-move-to-cart [value='Move to Cart']",

    SFL_WISHLISTED_SELECTOR: ".sc-action-move-to-wishlist [value='Move to Wish List']",

    SFL_SAVE_LATER_SELECTOR: ".sc-action-save-for-later [value='Move to Cart']"
};